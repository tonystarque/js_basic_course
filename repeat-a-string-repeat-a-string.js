//repeat a string
function repeatStringNumTimes(str, num) {
  var i = 1, newStr = "";
  while (i <= num) {
    if (num <= 0) {
      return "";
    } else {
    newStr += str;
    }
    i += 1;
  }
  return newStr;
}
repeatStringNumTimes("abc", 3);